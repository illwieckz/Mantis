# Mantis
This is the IRC bot found in the #unvanquished(-dev) channels on Libera.Chat.

## Disclaimer
**I wrote Mantis as a personal program.**
I open-sourced it on request and I don't plan on maintaining it beyond fixing critical bugs or making the changes that I need personally.
This is also one of my earlier C++ applications and may look a lot like C.

## Dependencies
* libircclient
* libconfig

## License
[GNU General Public License v3.0](LICENSE)
